package Data;

import javax.print.attribute.DateTimeSyntax;

/*
 * Entity Class For Table 'Vehicle'
 */
public class Vehicle {
	
	//Private Column Value Holders
	private int id = -1;
	private String name = "";
	private String description = "";
	private String img = "";
	private String transmission = "";
	private String feature1 = "";
	private String feature2 = "";
	private String feature3 = "";
	private String feature4 = "";
	private String feature5 = "";
	private int safetyRating = -1;
	private DateTimeSyntax date = null;
	private String company = "";
	
	//Vehicle Modeling
	private VehicleModel model =  new VehicleModel();
	
	
	
	//=====================================
	//Constructs --------------------------
	//=====================================
	
	public Vehicle() {} //<--Allow Empty
	
	//=====================================
	
	
	//=====================================
	//Getters -----------------------------
	//=====================================
		
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getImg() {
		return img;
	}
	
	public String getTransmission() {
		return transmission;
	}
	
	public String getFeature1() {
		return feature1;
	}
	
	public String getFeature2() {
		return feature2;
	}
	
	public String getFeature3() {
		return feature3;
	}
	
	public String getFeature4() {
		return feature4;
	}
	
	public String getFeature5() {
		return feature5;
	}
	
	public int getSafetyRating() {
		return safetyRating;
	}
	
	public DateTimeSyntax getDate() {
		return date;
	}
	
	public String getCompany() {
		return company;
	}
	
	//=====================================
	
	
	
	//=====================================
	//Setters -----------------------------
	//=====================================
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setImg(String img) {
		this.img = img;
	}
	
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}
	
	public void setFeature1(String feature1) {
		this.feature1 = feature1;
	}
	
	public void setFeature2(String feature2) {
		this.feature2 = feature2;
	}
	
	public void setFeature3(String feature3) {
		this.feature3 = feature3;
	}
	
	public void setFeature4(String feature4) {
		this.feature4 = feature4;
	}
	
	public void setFeature5(String feature5) {
		this.feature5 = feature5;
	}
	
	public void setSafetyRating(int safetyRating) {
		this.safetyRating = safetyRating;
	}
	
	public void setDate(DateTimeSyntax date) {
		this.date = date;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	//=====================================
	

} //EOC
