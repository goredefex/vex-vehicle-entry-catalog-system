package Presentation;


public class MainViewController {
		
	private MainView window; 
	
	//=====================================
	//Constructs --------------------------
	//=====================================
	
	public MainViewController() {} //<--allow empty
	
	//=====================================
	
	
	
	//=====================================
	//Functions ---------------------------
	//=====================================
		
	/*
	 * Launches The MainView Window
	 */
	public void Initialize() {
		
		try {
			this.window = new MainView();
			window.frmVexVehicle.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} //end function
	
	//=====================================
	
	
	

} //EOC
