package Presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.List;

public class MainView {

	
	//Public Access Members
	public JFrame frmVexVehicle;
	
	
	
	//=====================================
	//Constructs --------------------------
	//=====================================
	
	/**
	 * Create the application.
	 */
	public MainView() {
		initialize();
		
	} //end constructor

	//=====================================
	
	
	
	
	//=====================================
	//Functions ---------------------------
	//=====================================
		
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmVexVehicle = new JFrame();
		frmVexVehicle.setResizable(false);
		frmVexVehicle.setTitle("VEX - Vehicle Entry Catalog System");
		frmVexVehicle.setBounds(100, 100, 728, 338);
		frmVexVehicle.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JSplitPane mainContentSplitPane = new JSplitPane();
		mainContentSplitPane.setResizeWeight(0.25);
		frmVexVehicle.getContentPane().add(mainContentSplitPane, BorderLayout.CENTER);
		
		Panel searchResultsPanel = new Panel();
		mainContentSplitPane.setLeftComponent(searchResultsPanel);
		searchResultsPanel.setLayout(null);
		
		List searchResultsLitsBox = new List();
		searchResultsLitsBox.setBounds(0, 0, 156, 191);
		searchResultsPanel.add(searchResultsLitsBox);
	
	} //end function
	
	//=====================================
			
			
} //EOC
