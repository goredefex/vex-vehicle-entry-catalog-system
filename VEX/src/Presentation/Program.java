package Presentation;

import java.awt.EventQueue;

public class Program {	
	
	//=====================================
	//Entry Point -------------------------
	//=====================================
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MainViewController mV = new MainViewController();
				mV.Initialize();
			}
		});
	
	} //end entry
	
	//=====================================
	

} //EOC
